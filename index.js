import React from 'react'
import ReactDOM from 'react-dom/client'

const container = document.createElement('div');
document.body.appendChild(container);

const root = ReactDOM.createRoot(container);
root.render(<h1>Hello, world!</h1>);
